﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeDefenderBase : DefenderBase
{

    void Awake()
    {
        SetDefaultValues();
    }

    void SetDefaultValues()
    {
        unitMainAttackType = UnitAttackTypes.Melee;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<AttackerBase>())
        {
            AttackTarget(currentEnemy);
        }
    }
}


