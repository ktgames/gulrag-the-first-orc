﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderBase : UnitBase {

    public int deathReward;
    public Color color;

    // Use this for initialization
    void Awake() {
        SetDefaultValues();
    }

    void Update() {
        if (currentHealthPoints <= 0) {
            Death();
        } else {
            AttackTarget(currentEnemy);
        }
    }

    public void SetDefaultValues() {
        currentHealthPoints = startingHealthPoints;
        unitType = UnitTypes.Defender;
        deathValue = deathReward;
    }

    void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.GetComponent<UnitBase>() != null
                && collision is BoxCollider2D
                && collision.gameObject.GetComponent<UnitBase>().currentEnemy == this.gameObject
                && collision.gameObject.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
                && Vector2.Distance(collision.gameObject.transform.position, this.transform.position) < collision.gameObject.GetComponent<UnitBase>().attackRadius) {
            collision.gameObject.GetComponent<UnitBase>().attacking = true;
        }
    }

    //private void OnTriggerStay2D(Collider2D collision) {
    //    if (collision.gameObject.GetComponent<UnitBase>() != null
    //            && collision.gameObject.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker
    //            && collision.gameObject.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
    //            && collision is CircleCollider2D
    //            && collision.gameObject.GetComponent<UnitBase>().currentEnemy == null) {
    //        collision.gameObject.GetComponent<UnitBase>().enemyFound = true;
    //        collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
    //        collision.gameObject.GetComponent<UnitPathfinding>().enemyCollision = true;
    //        collision.gameObject.GetComponent<UnitPathfinding>().enemyTransform = this.transform;
    //    }
    //}

    public void AttackTarget(GameObject enemy) {
        if (enemy != null && Time.time > nextAttack) {
            nextAttack = Time.time + attackRate;
            StartCoroutine(AttackTheAttacker(enemy));
        }
    }

    IEnumerator AttackTheAttacker(GameObject attacker) {
        if (attacker.GetComponent<AttackerBase>().currentHealthPoints > 0 && currentHealthPoints > 0) {
            Animation("ATTACKING");
            AttackDamage(attacker, damageOutput);
            yield return new WaitForSecondsRealtime(1);
        } else {
            yield return new WaitForEndOfFrame();
        }
    }

    public bool Death() {
        //if (currentHealthPoints <= 0 && beingAttacked) {
            //currentEnemy.GetComponent<UnitBase>().currentEnemyDead = true;
            if (!deathValueAdded) {
                deathValueAdded = true;
                gameManager.UpdatePlayerCurrency(unitType, deathValue);
            }
            StartCoroutine(AnimationWithTimer("DEATH", 2f));
            Destroy(gameObject, 2f);
        //}
        return true;
    }

    void OnDrawGizmos() {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
