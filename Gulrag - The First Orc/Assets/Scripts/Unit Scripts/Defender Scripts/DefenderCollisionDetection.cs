﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderCollisionDetection : UnitCollisionDetectionBase {

    void OnTriggerStay2D(Collider2D collision) {
        if (this.name == Constants.BODY_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == null
                && !collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound = true;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = this.parentGameObject;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyCollision = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyTransform = this.parentGameObject.transform;
        } else if (this.name == Constants.BODY_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Ranged
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == null
                && !collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound = true;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = this.parentGameObject;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyCollision = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyTransform = this.parentGameObject.transform;
            this.transform.parent.GetComponent<UnitBase>().beingAttacked = true;
        } else if (this.name == Constants.BODY_COLLIDER
                && collision is BoxCollider2D
                && collision.gameObject.name == Constants.BODY_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker // did you mean for this to be the stealth class? 
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = true;
            this.transform.parent.GetComponent<UnitBase>().beingAttacked = true;
        }

        //if (collision.gameObject.GetComponent<UnitBase>() != null
        //        && collision.gameObject.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker
        //        && collision.gameObject.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
        //        && collision is CircleCollider2D
        //        && collision.gameObject.GetComponent<UnitBase>().currentEnemy == null
        //        && !collision.gameObject.GetComponent<UnitBase>().enemyFound) {
        //    collision.gameObject.GetComponent<UnitBase>().enemyFound = true;
        //    collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
        //    collision.gameObject.GetComponent<UnitPathfinding>().enemyCollision = true;
        //    collision.gameObject.GetComponent<UnitPathfinding>().enemyTransform = this.transform;
        //} else if (collision.gameObject.GetComponent<UnitBase>() != null
        //        && collision.gameObject.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Attacker
        //        && collision.gameObject.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Ranged
        //        && collision is CircleCollider2D
        //        && collision.gameObject.GetComponent<UnitBase>().currentEnemy == null
        //        && !collision.gameObject.GetComponent<UnitBase>().enemyFound) {
        //    collision.gameObject.GetComponent<UnitBase>().enemyFound = true;
        //    collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
        //    collision.gameObject.GetComponent<UnitBase>().attacking = true;
        //    collision.gameObject.GetComponent<UnitPathfinding>().enemyCollision = true;
        //    collision.gameObject.GetComponent<UnitPathfinding>().enemyTransform = this.transform;
        //    beingAttacked = true;
        //}
        //else if (collision.gameObject.GetComponent<UnitBase>() != null
        //        && collision is BoxCollider2D
        //        && collision.gameObject.GetComponent<UnitBase>().currentEnemy == this.gameObject
        //        && collision.gameObject.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
        //        && Vector2.Distance(collision.gameObject.transform.position, this.transform.position) < collision.gameObject.GetComponent<UnitBase>().attackRadius) {
        //    collision.gameObject.GetComponent<UnitBase>().attacking = true;
        //}
    }
}
