﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitBase : MonoBehaviour {

    public enum UnitTypes { Attacker, Defender }
    public UnitTypes unitType;

    public enum UnitAttackTypes { None, Melee, Ranged, Stealth };
    public UnitAttackTypes unitMainAttackType;
    public UnitAttackTypes unitSecondaryAttackType;

    public float startingHealthPoints;
    public float currentHealthPoints;
    [HideInInspector] public int deathValue;
    [HideInInspector] public bool deathValueAdded = false;

    public bool enemyFound = false;
    public GameObject currentEnemy;
    public bool currentEnemyDead = false;
    public List<GameObject> beingAttackedBy;
    public List<GameObject> futureEnemies;
    public bool hasFutureEnemies;

    public int threatLevel;
    public float attackRadius;
    public float damageOutput;
    public float attackRate = 2f;
    public float attackCooldown = 1f;
    [HideInInspector] public float nextAttack = 0.0f;

    public bool attacking = false;
    public bool beingAttacked = false;

    public GameManager gameManager;
    [HideInInspector] public Animator animator;

    public GameObject closestEnemy;

    //public Color color;

    void Awake() {
        GameObject gManager = GameObject.Find("GameManager");
        gameManager = gManager.GetComponent<GameManager>();
        animator = this.GetComponent<Animator>();
    }

    public float AttackDamage(GameObject enemy, float damage) {
        UnitBase enemyUnitBase = enemy.GetComponent<UnitBase>();
        enemyUnitBase.currentHealthPoints -= damage;
        return enemyUnitBase.currentHealthPoints;
    }

    public void Animation(string animationName) {
        if (animator == null) {
            animator = this.GetComponent<Animator>();
        }
        animator.SetTrigger(animationName);
    }

    public IEnumerator AnimationWithTimer(string animationName, float timer) {
        Animation(animationName);
        yield return new WaitForSecondsRealtime(timer);
    }

    public bool NotAttackingOrBeingAttacked() {
        return (!attacking && !beingAttacked) ? true : false;
    }
}
