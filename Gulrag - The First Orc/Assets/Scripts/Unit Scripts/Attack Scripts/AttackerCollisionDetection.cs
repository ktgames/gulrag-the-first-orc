﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerCollisionDetection : UnitCollisionDetectionBase {

    /*void OnTriggerEnter2D(Collider2D collision) {
        if (this.name == Constants.BODY_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == null
                && !collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound = true;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = this.parentGameObject;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyCollision = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().enemyTransform = this.parentGameObject.transform;    //This line right here may add for the ability of the defenders to walk and attack our playres if the pathfinding scripts are added to them? 
        } else if (this.name == Constants.BODY_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Ranged
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == null
                && !collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound = true;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = this.parentGameObject;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = true;
            this.transform.parent.GetComponent<UnitBase>().beingAttacked = true;
        } else if (this.name == Constants.BODY_COLLIDER
                && collision is BoxCollider2D
                && collision.gameObject.name == Constants.BODY_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Melee) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = true;
            this.transform.parent.GetComponent<UnitBase>().beingAttacked = true;
        }
    }*/

    void OnTriggerStay2D(Collider2D collision) {
        if (this.name == Constants.BODY_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Ranged
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == null
                && !collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().enemyFound = true;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = this.parentGameObject;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = true;
            this.transform.parent.GetComponent<UnitBase>().beingAttacked = true;
        } else if (this.name == Constants.DETECTION_COLLIDER
                && collision is BoxCollider2D
                && collision.gameObject.name == Constants.BODY_COLLIDER
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && this.transform.parent != null
                && this.transform.parent.GetComponent<UnitBase>() != null
                && this.transform.parent.GetComponent<UnitBase>().currentEnemy != null
                && this.transform.parent.GetComponent<UnitBase>().currentEnemy != collision.gameObject.GetComponent<DefenderCollisionDetection>().parentGameObject) {
            if (this.transform.parent.GetComponent<UnitBase>().closestEnemy == null) {
                this.transform.parent.GetComponent<UnitBase>().closestEnemy = collision.gameObject.GetComponent<DefenderCollisionDetection>().parentGameObject;
            } else if (this.transform.parent.GetComponent<UnitBase>().closestEnemy != null
                    && this.transform.parent.GetComponent<UnitBase>().currentEnemy != collision.gameObject.transform.parent
                    && Vector2.Distance(new Vector2(this.transform.position.x, this.transform.position.y), new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y)) < 
                        Vector2.Distance(new Vector2(this.transform.position.x, this.transform.position.y), 
                            new Vector2(this.transform.parent.GetComponent<UnitBase>().closestEnemy.transform.position.x, 
                                this.transform.parent.GetComponent<UnitBase>().closestEnemy.transform.position.y))) {
                this.transform.parent.GetComponent<UnitBase>().closestEnemy = collision.gameObject.GetComponent<DefenderCollisionDetection>().parentGameObject;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>() != null
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().unitType == UnitBase.UnitTypes.Defender
                && collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy == this.parentGameObject) {
            collision.gameObject.transform.parent.GetComponent<UnitBase>().currentEnemy = null;
            collision.gameObject.transform.parent.GetComponent<UnitBase>().attacking = false;
        }
        if (this.transform.parent.GetComponent<UnitBase>().closestEnemy != null
                && this.transform.parent.GetComponent<UnitBase>().closestEnemy == collision.GetComponent<DefenderCollisionDetection>().parentGameObject) {
            this.transform.parent.GetComponent<UnitBase>().closestEnemy = null;
        }
    }
}