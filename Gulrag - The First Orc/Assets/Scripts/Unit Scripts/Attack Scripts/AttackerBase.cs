﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerBase : UnitBase {

    [HideInInspector] public UnitPathfinding unitPathfinding;
    public float unitCost;
    public int deathCost;
    public Color color;

    // Use this for initialization
    void Awake() {
        SetDefaultValues();
    }

    void Update() {
        if (currentEnemy != null && attacking) {
            StartCoroutine(AttackDefender());
        } else if (currentEnemy == null && attacking) {
            ResetAfterKillingEnemy();
        } else if (currentEnemy == closestEnemy) {
            closestEnemy = null;
        }
        SetAnimationFromPathfinding(this.gameObject.GetComponent<UnitPathfinding>().unitDirection);
    }

    public void SetDefaultValues() {
        currentHealthPoints = startingHealthPoints;
        unitType = UnitTypes.Attacker;
        unitPathfinding = this.GetComponent<UnitPathfinding>();
        deathValue = deathCost;
    }

    IEnumerator AttackDefender() {
        yield return new WaitForEndOfFrame();
    }

    IEnumerator AttackDefender(GameObject defender) {
        yield return new WaitForEndOfFrame();
    }

    public void ResetAfterKillingEnemy() {
        StopAllCoroutines();
        attacking = false;
        enemyFound = false;
        this.gameObject.GetComponent<UnitPathfinding>().targetPathComplete = true;
        this.gameObject.GetComponent<UnitPathfinding>().ResetSpeed();
    }

    public void SetAnimationFromPathfinding(Constants.PathfindingDirections unitDirection) {
        if (unitDirection == Constants.PathfindingDirections.Up) {
            // unit moving up animation
        } else if (unitDirection == Constants.PathfindingDirections.Down) {
            // unit moving down animation
        } else if (unitDirection == Constants.PathfindingDirections.Left) {
            // unit moving left animation
        } else if (unitDirection == Constants.PathfindingDirections.Right) {
            // unit moving right animation
        } else {
            // default animation
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
