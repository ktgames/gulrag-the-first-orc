﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealthMeleeAttackerBase : AttackerBase {

    public float trapDisarmDamage;

    public bool trapFound = false;
    public GameObject currentTrap;

    public bool disarmingTrap = false;

    void Update() {
        if (trapFound && disarmingTrap && currentTrap != null) {
            StartCoroutine(DisarmTrap());
        } else if (trapFound && disarmingTrap && currentTrap == null) {
            ResetAfterTrapDisarmed();
        }
    }

    public IEnumerator DisarmTrap() {
        unitPathfinding.speed = 0;
        //Animation("DISARMING");
        DisarmDamage(currentTrap, trapDisarmDamage);
        yield return new WaitForSecondsRealtime(attackCooldown);
    }

    public float DisarmDamage(GameObject trap, float damage) {
        TrapBaseScript trapBaseScript = trap.GetComponent<TrapBaseScript>();
        trapBaseScript.trapHealth -= trapDisarmDamage;
        return trapBaseScript.trapHealth;
    }

    public void ResetAfterTrapDisarmed() {
        trapFound = false;
        disarmingTrap = false;
        this.GetComponent<UnitPathfinding>().pathfindingPaused = false;
        this.GetComponent<UnitPathfinding>().trapPathComplete = true;
        this.GetComponent<UnitPathfinding>().ResetSpeed();
    }
}
