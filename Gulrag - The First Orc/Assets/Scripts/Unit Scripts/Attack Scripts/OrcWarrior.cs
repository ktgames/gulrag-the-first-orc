﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;


public class OrcWarrior : MeleeAttackerBase {

    public AnimationReferenceAsset walking;
    public AnimationReferenceAsset idle;
    public AnimationReferenceAsset attackingAnimation; 
    SkeletonAnimation skeletonAnimation;

    //public int orcWarriorCost = 100; TODO get the value from the bottom class eventually for currency.
    //TODO Get animation states working via movement from unit pathfinding / currentEnemies. 
    //private void Awake()
    //{
        
    //}


    //void Start() {
    //    SetDefaultValues();
    //    unitPathfinding = GetComponent<UnitPathfinding>();
    //    skeletonAnimation = GetComponent<SkeletonAnimation>();
    //    skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
    //}

    //private void Update()
    //{
    //    SetWalkingAnimation();
    //    Debug.Log(unitPathfinding.speed);
    //}

    void SetWalkingAnimation()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            skeletonAnimation.AnimationState.SetAnimation(0, idle, false);
            skeletonAnimation.AnimationState.SetAnimation(0, walking, true);
        }

    }

    void SetAttackAnimation()
    {
        if (attacking == true)
        {

            skeletonAnimation.AnimationState.SetAnimation(0, attackingAnimation, true);
        }

    }

}
