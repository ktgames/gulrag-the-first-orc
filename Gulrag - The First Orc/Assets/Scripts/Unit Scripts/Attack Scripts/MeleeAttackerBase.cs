﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackerBase : AttackerBase {

    void Update() {
        if (currentEnemy != null && attacking) {
            StartCoroutine(AttackDefender());
        } else if (currentEnemy == null && attacking) {
            ResetAfterKillingEnemy();
        } else if (currentEnemy == null && closestEnemy != null && !attacking) {
            CheckForCurrentEnemyCollision();
        } else if (currentEnemy == closestEnemy) {
            closestEnemy = null;
        }
    }

    void SetDefaultValues() {
        unitMainAttackType = UnitAttackTypes.Melee;
    }

    public bool Death() {
        if (currentHealthPoints <= 0 && beingAttacked) {
            currentEnemy.GetComponent<UnitBase>().currentEnemyDead = true;
            animator.SetTrigger("DEATH");
            Destroy(gameObject, 2f);
        }
        return true;
    }

    //void OnTriggerEnter2D(Collider2D collision) {
    //    CheckAndSetCurrentEnemy(collision);
    //}

    //void OnTriggerStay2D(Collider2D collision) {
    //    Debug.Log("collision.name: " + collision.name);
    //    if (collision is BoxCollider2D
    //            && collision.gameObject == currentEnemy) {
    //        Debug.Log("collision.gameObject: " + collision.gameObject);
    //        if (!attacking) {
    //            attacking = true;
    //        }
    //        StartCoroutine(AttackDefender(currentEnemy));
    //    }
    //}

    //void CheckAndSetCurrentEnemy(Collider2D collision) {
    //    if (collision.gameObject.tag.ToLower() == "defender"
    //            && collision is CircleCollider2D
    //            && collision.gameObject.GetComponent<UnitBase>() != null
    //            && collision.gameObject.GetComponent<UnitBase>().currentEnemy == null
    //            && currentEnemy == null) {
    //        collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
    //        collision.gameObject.GetComponent<UnitBase>().attacking = true;
    //        beingAttacked = true;
    //    }
    //}

    public IEnumerator AttackDefender() {
        unitPathfinding.speed = 0;
        Animation("ATTACKING");
        AttackDamage(currentEnemy, damageOutput);
        yield return new WaitForSecondsRealtime(attackCooldown);
    }

    public IEnumerator AttackDefender(GameObject defender) {
        if (defender.GetComponent<UnitBase>().currentHealthPoints > 0 && attacking) {
            Debug.Log("HERE 2");
            unitPathfinding.speed = 0;
            Animation("ATTACKING");
            AttackDamage(defender, damageOutput);
            yield return new WaitForSecondsRealtime(attackCooldown);
        }
        //else if (defender == null || (defender.GetComponent<UnitBase>().currentHealthPoints <= 0 && attacking)) {
        //    unitPathfinding.speed = 5;
        //    attacking = false;
        //    beingAttacked = false;
        //    unitPathfinding.targetPathComplete = true;
        //    yield return new WaitForEndOfFrame();
        //}
    }

    public void ResetAfterKillingEnemy() {
        StopAllCoroutines();
        attacking = false;
        enemyFound = false;
        this.gameObject.GetComponent<UnitPathfinding>().targetPathComplete = true;
        this.gameObject.GetComponent<UnitPathfinding>().ResetSpeed();
        //CheckForCurrentEnemyCollision();
    }

    public void CheckForCurrentEnemyCollision() {
        if (closestEnemy != null) {
            currentEnemy = closestEnemy;
            closestEnemy = null;
            enemyFound = true;
            this.gameObject.GetComponent<UnitPathfinding>().enemyCollision = true;
            this.gameObject.GetComponent<UnitPathfinding>().enemyTransform = closestEnemy.transform;
        }
    }

    //void OnTriggerEnter2D(Collider2D collision) {
    //    if (collision.gameObject.tag == "Defender" && collision is CircleCollider2D 
    //            && !unitPathfinding.enemyCollision && !unitPathfinding.onTargetPath
    //            && currentEnemy == null) {
    //        if (collision.gameObject.GetComponent<UnitBase>().currentEnemy == null) {
    //            collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
    //            collision.gameObject.GetComponent<UnitBase>().attacking = true;
    //        } else {
    //            collision.gameObject.GetComponent<UnitBase>().futureEnemies.Add(this.gameObject);
    //        }

    //        currentEnemy = collision.gameObject;
    //        currentEnemyDead = false;
    //        beingAttacked = true;
    //        unitPathfinding.enemyCollision = true;
    //        unitPathfinding.enemyTransform = collision.gameObject.transform;
    //    } else if (collision.gameObject.tag == "Defender" && collision is CircleCollider2D
    //            && collision.gameObject.GetComponent<UnitBase>().currentEnemy == null
    //            && unitPathfinding.onTargetPath && currentEnemy != null
    //            && currentEnemy != null) {
    //        if (collision.gameObject.GetComponent<UnitBase>().currentEnemy == null) {
    //            collision.gameObject.GetComponent<UnitBase>().currentEnemy = this.gameObject;
    //            collision.gameObject.GetComponent<UnitBase>().attacking = true;
    //        } else {
    //            collision.gameObject.GetComponent<UnitBase>().futureEnemies.Add(this.gameObject);
    //        }
    //        beingAttacked = true;
    //        futureEnemies.Add(collision.gameObject);
    //        hasFutureEnemies = true;
    //    }
    //}

    //void OnTriggerStay2D(Collider2D collision) {
    //    if (collision.gameObject == currentEnemy && collision is BoxCollider2D && currentHealthPoints >= 0 && Time.time > nextAttack) {
    //        currentEnemy.gameObject.GetComponent<UnitBase>().beingAttacked = true;
    //        attacking = true;
    //        nextAttack = Time.time + attackRate;
    //        StartCoroutine(AttackDefender(currentEnemy));
    //    } else if (currentEnemy == null) {
    //        unitPathfinding.speed = 5;
    //        attacking = false;
    //        if (futureEnemies.Count == 0) {
    //            beingAttacked = false;
    //        }
    //        unitPathfinding.targetPathComplete = true;
    //        StopAllCoroutines();
    //    }
    //}
}
