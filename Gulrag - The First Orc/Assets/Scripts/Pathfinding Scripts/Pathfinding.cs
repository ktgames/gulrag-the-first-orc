﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    PathRequestManager requestManager;
    Grid grid;

    void Awake() {
        requestManager = GetComponent<PathRequestManager>();
        grid = GetComponent<Grid>();
    }

    public void StartFindPath(Vector2 startPosition, Vector2 targetPosition, bool isMultiPath) {
        StartCoroutine(FindPath(startPosition, targetPosition, isMultiPath));
    }
    
    IEnumerator FindPath(Vector2 startPosition, Vector2 targetPosition, bool isMultiPath) {

        //Stopwatch sw = new Stopwatch();
        //sw.Start();

        Vector2[] waypoints = new Vector2[0];
        bool pathSuccess = false;

        Node startNode = grid.NodeFromWorldPoint(startPosition);
        Node targetNode = grid.NodeFromWorldPoint(targetPosition);
        
        if (startNode.walkable && targetNode.walkable) {
            Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);

            while (openSet.Count > 0) {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode) {
                    //sw.Stop();
                    //print("Path found time: " + sw.ElapsedMilliseconds + "ms");
                    pathSuccess = true;
                    break;
                }

                foreach (Node neighbor in grid.GetNeighbors(currentNode)) {
                    if (!neighbor.walkable || closedSet.Contains(neighbor)) {
                        continue;
                    }

                    int newMovementCostToNeighbor = currentNode.gCost + GetDistance(currentNode, neighbor);
                    if (newMovementCostToNeighbor < neighbor.gCost || !openSet.Contains(neighbor)) {
                        neighbor.gCost = newMovementCostToNeighbor;
                        neighbor.hCost = GetDistance(neighbor, targetNode);
                        neighbor.parent = currentNode;

                        if (!openSet.Contains(neighbor)) {
                            openSet.Add(neighbor);
                        }
                    }
                }
            }
            yield return null;
            if (pathSuccess) {
                waypoints = RetracePath(startNode, targetNode);
            }
            requestManager.FinishedProcessingPath(waypoints, pathSuccess);
        }
    }

    Vector2[] RetracePath(Node startNode, Node endNode) {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector2[] waypoints = ConvertPath(path);
        Array.Reverse(waypoints);
        return waypoints;

        //path.Reverse();
        //return path;
    }

    Vector2[] ConvertPath(List<Node> path) {
        List<Vector2> pathPoints = new List<Vector2>();
        Vector2 oldPoint = Vector2.zero;

        for (int i = 1; i < path.Count; i++) {
            Vector2 newPoint = new Vector2(path[i].gridX, path[i].gridY);
            if (newPoint != oldPoint) {
                pathPoints.Add(path[i].worldPosition);
            }
            oldPoint = newPoint;
        }

        return pathPoints.ToArray();
    }

    Vector2[] SimplifyPath(List<Node> path) {
        List<Vector2> waypoints = new List<Vector2>();
        Vector2 directionOld = Vector2.zero;

        for (int i = 1; i < path.Count; i++) {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld) {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    // TO REMOVE DIAGONAL MOVEMENT, REMOVE HERE AS WELL
    public int GetDistance(Node nodeA, Node nodeB) {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (!Grid.diagonalMovement) {
            if (dstX > dstY) {
                return 14 * dstY + 10 * dstX - dstY;
            }
            return 14 * dstX + 10 * dstY - dstX;
        }
        return 10 * (dstX + dstY);
    }
}
