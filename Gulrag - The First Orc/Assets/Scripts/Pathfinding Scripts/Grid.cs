﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Grid : MonoBehaviour {

    /***** VARIABLES *****/
    
    // Variables that can be set publicly
    public bool displayGridGismos;
    public LayerMask unwalkableMask;
    public Tilemap obstacles;
    public Tilemap goals;
    public static List<Vector2> goalPositions;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    public bool disableDiagonalMovement;

    // Local Class Variables
    public float nodeDiameter;
    // Grid Node sizes = world scale / 10
    public int gridSizeX, gridSizeY;
    public Node[,] grid;
    public Dictionary<Vector2, Node> gridPositions;

    // Static Variables for use in other Classes
    public static Vector2 worldSize;
    public static float nDiameter;
    public static float nRadius;
    public static Node[,] nGrid;
    public static Dictionary<Vector2, Node> gridPositionsMap;
    public static bool diagonalMovement;

    /***** METHODS *****/

    // Sets local variables and instantiates grid
    void Awake() {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
        SetStaticVariables();
    }

    // Sets static variables for use in other classes
    void SetStaticVariables() {
        worldSize = gridWorldSize;
        nRadius = nodeRadius;
        nDiameter = nodeDiameter;
        nGrid = grid;
        gridPositionsMap = gridPositions;
        diagonalMovement = disableDiagonalMovement;
    }

    // Public method to get the max grid size
    public int MaxSize {
        get {
            return gridSizeX * gridSizeY;
        }
    }

    // Creates the grid for pathfinding
    void CreateGrid() {
        grid = new Node[gridSizeX, gridSizeY];
        gridPositions = new Dictionary<Vector2, Node>();
        goalPositions = new List<Vector2>();
        Vector2 worldBottomLeft = (Vector2.left * gridWorldSize.x / 2) - (Vector2.up * gridWorldSize.y / 2);

        // Loops through all grid locations and creates the grid based on
        // * whether the grid location is "walkable" or not
        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                Vector2 worldPoint = worldBottomLeft + Vector2.right * (x * nodeDiameter + nodeRadius) + Vector2.up * (y * nodeDiameter + nodeRadius);
                Vector2 worldPointFloor = new Vector2(Mathf.FloorToInt(worldPoint.x), Mathf.FloorToInt(worldPoint.y));

                bool walkable = true;

                // Returns a walkable boolean based on if an unwalkable tile is found in that location
                if (obstacles.GetTile(new Vector3Int((int)worldPointFloor.x, (int)worldPointFloor.y, 0)) != null) {
                    walkable = false;
                } else if (goals.GetTile(new Vector3Int((int)worldPointFloor.x, (int)worldPointFloor.y, 0)) != null) {
                    goalPositions.Add(worldPoint);
                }

                grid[x, y] = new Node(walkable, worldPoint, x, y);
                gridPositions.Add(worldPointFloor, grid[x, y]);
            }
        }
    }

    // Returns all neighboring grid nodes to the one passed in
    public List<Node> GetNeighbors(Node node) {
        List<Node> neighbors = new List<Node>();

        // Loops through all neighboring nodes
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0) {
                    continue;
                }

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                // THIS IS WHERE YOU CAN TURN OFF DIAGONAL MOVEMENT
                if (checkX >= 0 && checkX < gridSizeX
                    && checkY >= 0 && checkY < gridSizeY
                    && NotDiagonal(x, y)) {
                    neighbors.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbors;
    }

    // Used to turn off diagonal pathfinding movement
    public bool NotDiagonal(int checkX, int checkY) {
        bool diagonalCheck = true;

        if (disableDiagonalMovement) {
            if (checkX == -1 && (checkY == -1 || checkY == 1)) {
                diagonalCheck = false;
            } else if (checkX == 1 && (checkY == -1 || checkY == 1)) {
                diagonalCheck = false;
            }
        }

        return diagonalCheck;
    }

    // Finds the Node given a specific world point
    public Node NodeFromWorldPoint(Vector2 worldPosition) {
        Vector2 worldPositionFloor = new Vector2(Mathf.FloorToInt(worldPosition.x), Mathf.FloorToInt(worldPosition.y));

        Node node;

        gridPositions.TryGetValue(worldPositionFloor, out node);

        return node;
    }

    void OnDrawGizmos() {
        Gizmos.DrawWireCube(transform.position, new Vector2(gridWorldSize.x, gridWorldSize.y));
        if (grid != null && displayGridGismos) {
            foreach (Node n in grid) {
                Gizmos.color = (n.walkable) ? Color.white : Color.red;
                Gizmos.DrawCube(n.worldPosition, Vector2.one * (nodeDiameter - 0.1f));
            }
        }
    }
}
