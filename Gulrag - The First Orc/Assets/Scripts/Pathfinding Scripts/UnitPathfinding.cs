﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Tilemaps;

public class UnitPathfinding : MonoBehaviour {

    public float speed; // TODO change the speed to be derived entirely in code so that we can set animation states from unit speed. 
    public float initialSpeed;

    public Constants.PathfindingDirections unitDirection;

    public UnitBase.UnitTypes unitType;
    public UnitBase.UnitAttackTypes unitMainAttackType;
    public UnitBase.UnitAttackTypes unitSecondaryAttackType;

    public bool useTilemapAsTarget;
    public bool useGameobjectAsTarget;
    //public Transform target;
    //public Tilemap goals;
    public Color goalPathColor;
    public Color targetPathColor;
    public Color trapPathColor;
    public Vector2[] currentPath;
    public Vector2[] goalPath;
    public Vector2[] targetPath;
    public Vector2[] trapPath;
    public List<Vector2[]> potentialGoalPaths;
    public int targetIndex = 0;
    public Vector2 startPosition;
    public bool pathFound = false;
    public bool goalPathGenInProgress = false;
    public bool goalPathFound = false;
    public bool targetPathGenInProgress = false;
    public bool targetPathFound = false;
    public bool trapPathGenInProgress = false;
    public bool trapPathFound = false;

    public bool startPathfinding = false;
    public bool onGoalPath = false;
    public bool onTargetPath = false;
    public bool onTrapPath = false;

    public bool goalPathComplete = false;
    public bool targetPathComplete = false;
    public bool trapPathComplete = false;

    public bool pathfindingPaused = false;

    public bool enemyCollision = false;
    public Transform enemyTransform;
    public bool trapCollision = false;
    public Transform trapTransform;

    public GameManager gameManager;
    public Grid grid;

    public void Start() {
        GameObject aStar = GameObject.Find("A-Star");
        grid = (Grid)aStar.GetComponent(typeof(Grid));
        initialSpeed = speed;
        unitType = this.gameObject.GetComponent<UnitBase>().unitType;
        unitMainAttackType = this.gameObject.GetComponent<UnitBase>().unitMainAttackType;
        unitSecondaryAttackType = this.gameObject.GetComponent<UnitBase>().unitSecondaryAttackType;
        SetUnitDirection(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x, transform.position.y), true);
        RequestAllGoalPaths((Vector2)transform.position);
    }

    public void Update() {
        UnitPathHandler();
        if (!startPathfinding && currentPath != null && Input.GetButtonDown("Jump")) {
            StopCoroutine(FollowPath(currentPath));
            StartCoroutine(FollowPath(currentPath));
            startPathfinding = true;
            SetUnitDirection(new Vector2(transform.position.x, transform.position.y), currentPath[targetIndex], false);
        }
        if (currentPath != null && targetIndex < currentPath.Length) {
            SetUnitDirection(new Vector2(transform.position.x, transform.position.y), currentPath[targetIndex], false);
        }
    }

    public void UnitPathHandler() {
        if (!startPathfinding) {
            currentPath = goalPath;
            onGoalPath = true;
        } else if (unitType == UnitBase.UnitTypes.Attacker && unitMainAttackType == UnitBase.UnitAttackTypes.Melee) {
            MeleeAttackUnitPathHandler();
        } else if (unitType == UnitBase.UnitTypes.Attacker && unitMainAttackType == UnitBase.UnitAttackTypes.Ranged) {
            RangedAttackUnitPathHandler();
        } else if (unitType == UnitBase.UnitTypes.Attacker && unitMainAttackType == UnitBase.UnitAttackTypes.Stealth) {
            StealthAttackUnitPathHandler();
        }
    }

    public void MeleeAttackUnitPathHandler() {
        if (onGoalPath && !onTargetPath && enemyCollision && !targetPathGenInProgress && !targetPathFound
                && this.GetComponent<UnitBase>().currentEnemy != null) {
            InitiateTargetPathfinding(this.GetComponent<UnitBase>().currentEnemy.transform, true);
        } else if (onGoalPath && !onTargetPath && enemyCollision && !targetPathGenInProgress && targetPathFound) {
            ChangeToTargetPath(true);
        } else if (!onGoalPath && onTargetPath && !goalPathComplete && targetPathComplete && !goalPathGenInProgress && !goalPathFound) {
            InitiateFindNewGoalPath();
        } else if (!onGoalPath && onTargetPath && !goalPathComplete && targetPathComplete && !goalPathGenInProgress && goalPathFound) {
            ChangeToGoalPath();

            //TODO I keep getting a null reference exception and I think the parameters are not being met in this melee attack unit pathhandler. It is saying that enemyCollision = null which means neither true or false. It is getting lost
            //Somewhere in the unitpathfinding i think. 

        }
    }

    public void RangedAttackUnitPathHandler() {
        if (onGoalPath && enemyCollision && !pathfindingPaused
                && this.GetComponent<UnitBase>().currentEnemy != null) {
            PausePathfinding(true);
        } else if (onGoalPath && !enemyCollision && pathfindingPaused
                && this.GetComponent<UnitBase>().currentEnemy == null) {
            UnpausePathfinding(true);
        }
    }

    void StealthAttackUnitPathHandler() {
        if (onGoalPath && !onTrapPath && trapCollision && !trapPathGenInProgress && !trapPathFound
                && this.GetComponent<StealthMeleeAttackerBase>().currentTrap != null) {
            InitiateTargetPathfinding(this.GetComponent<StealthMeleeAttackerBase>().currentTrap.transform, false);
        } else if (onGoalPath && !onTrapPath && trapCollision && !trapPathGenInProgress && trapPathFound) {
            ChangeToTargetPath(false);
        } else if (!onGoalPath && onTrapPath && !goalPathComplete && trapPathComplete && !goalPathGenInProgress && !goalPathFound && pathfindingPaused) {
            PauseStealthMeleePathfinding();
        } else if (!onGoalPath && onTrapPath && !goalPathComplete && trapPathComplete && !goalPathGenInProgress && !goalPathFound && !pathfindingPaused) {
            InitiateFindNewGoalPath();
        } else if (!onGoalPath && onTrapPath && !goalPathComplete && trapPathComplete && !goalPathGenInProgress && goalPathFound && !pathfindingPaused) {
            ChangeToGoalPath();
        }
    }

    public void PausePathfinding(bool setBooleans) {
        speed = 0;
        if (setBooleans) {
            enemyCollision = false;
            pathfindingPaused = true;
        }
    }

    public void UnpausePathfinding(bool setBooleans) {
        speed = initialSpeed;
        if (setBooleans) {
            pathfindingPaused = false;
        }
    }

    public void ResetSpeed() {
        speed = initialSpeed;
    }

    void PauseStealthMeleePathfinding() {
        speed = 0;
        trapCollision = false;
        pathfindingPaused = true;
    }

    void UnpauseStealthMeleePathfinding() {
        speed = initialSpeed;
        pathfindingPaused = false;
    }

    void InitiateTargetPathfinding(Transform targetTransform, bool isEnemy) {
        StopAllCoroutines();
        if (!isEnemy) {
            trapTransform = targetTransform;
            RequestTrapPath(this.transform.position, trapTransform.position);
        } else if (isEnemy) {
            enemyTransform = targetTransform;
            RequestTargetPath(this.transform.position, enemyTransform.position);
        }
    }

    void ChangeToTargetPath(bool isEnemy) {
        if (!isEnemy) {
            currentPath = trapPath;
            targetIndex = 0;
            speed = 5f;
            trapCollision = false;
            onGoalPath = false;
            goalPathFound = false;
            trapPathComplete = false;
            onTrapPath = true;
        } else if (isEnemy) {
            currentPath = targetPath;
            this.GetComponent<UnitBase>().futureEnemies.RemoveAt(0);
            targetIndex = 0;
            speed = 5f;
            this.GetComponent<UnitBase>().currentEnemyDead = false;
            this.GetComponent<UnitBase>().currentEnemy.GetComponent<UnitBase>().beingAttacked = true;
            enemyCollision = false;
            onGoalPath = false;
            goalPathFound = false;
            targetPathComplete = false;
            onTargetPath = true;
        }
        StartCoroutine(FollowPath(currentPath));
    }

    public void InitiateFindNewGoalPath() {
        StopAllCoroutines();
        RequestAllGoalPaths((Vector2)transform.position);
    }

    public void ChangeToGoalPath() {
        targetIndex = 0;
        currentPath = goalPath;
        onTargetPath = false;
        onTrapPath = false;
        targetPathFound = false;
        targetPathComplete = false;
        trapPathFound = false;
        trapPathComplete = false;
        onGoalPath = true;
        StartCoroutine(FollowPath(currentPath));
    }

    public void RequestAllGoalPaths(Vector2 sPos) {
        goalPathGenInProgress = true;
        potentialGoalPaths = new List<Vector2[]>();

        foreach (Vector2 goalPosition in Grid.goalPositions) {
            PathRequestManager.RequestPath(sPos, goalPosition, OnMultiPathFound);
        }
    }

    public void OnMultiPathFound(Vector2[] newPath, bool pathSuccessful) {
        if (pathSuccessful) {
            potentialGoalPaths.Add(newPath);
            if (potentialGoalPaths.Count == Grid.goalPositions.Count) {
                goalPath = FindShortestPath(potentialGoalPaths);
                goalPathGenInProgress = false;
                goalPathFound = true;
            }
        }
    }

    public void RequestTargetPath(Vector2 sPos, Vector2 tPos) {
        targetPathGenInProgress = true;
        PathRequestManager.RequestPath(sPos, tPos, OnTargetPathFound);
    }

    public void OnTargetPathFound(Vector2[] newPath, bool pathSuccessful) {
        if (pathSuccessful) {
            targetPath = newPath;
            targetPathGenInProgress = false;
            targetPathFound = true;
        }
    }

    void RequestTrapPath(Vector2 sPos, Vector2 tPos) {
        trapPathGenInProgress = true;
        PathRequestManager.RequestPath(sPos, tPos, OnTrapPathFound);
    }

    public void OnTrapPathFound(Vector2[] newPath, bool pathSuccessful) {
        if (pathSuccessful) {
            trapPath = newPath;
            trapPathGenInProgress = false;
            trapPathFound = true;
        }
    }

    public Vector2[] FindShortestPath(List<Vector2[]> paths) {
        Vector2[] shortestPath = paths[0];

        foreach (Vector2[] singlePath in paths) {
            shortestPath = singlePath.Length < shortestPath.Length ? singlePath : shortestPath;
        }

        return shortestPath;
    }

    public IEnumerator FollowPath(Vector2[] path) {
        Vector2 currentWaypoint = path[0];
        if (onGoalPath) {
            goalPathFound = false;
        } else if (onTargetPath) {
            targetPathFound = false;
        } else if (onTrapPath) {
            trapPathFound = false;
        }
        while (true) {
            if (transform.position.x == currentWaypoint.x && transform.position.y == currentWaypoint.y) {
                targetIndex++;
                if (targetIndex >= path.Length) {
                    if (onGoalPath) {
                        gameManager.SetWinText();
                        goalPathComplete = true;
                    } else if (onTargetPath) {
                        targetPathComplete = true;
                    } else if (onTrapPath) {
                        trapPathComplete = true;
                    }
                    yield break;
                }
                currentWaypoint = path[targetIndex];
            }
            transform.position = Vector2.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
            yield return null;
        }
    }

    public void SetUnitDirection(Vector2 currentPosition, Vector2 waypointPosition, bool startPosition) {
        if (startPosition) {
            if (unitType == UnitBase.UnitTypes.Attacker) {
                unitDirection = Constants.PathfindingDirections.Right;
            } else if (unitType == UnitBase.UnitTypes.Defender) {
                unitDirection = Constants.PathfindingDirections.Left;
            }
        } else {
            if (currentPosition.y < waypointPosition.y) {
                unitDirection = Constants.PathfindingDirections.Up;
            } else if (currentPosition.y > waypointPosition.y) {
                unitDirection = Constants.PathfindingDirections.Down;
            } else if (currentPosition.x < waypointPosition.x) {
                unitDirection = Constants.PathfindingDirections.Right;
            } else if (currentPosition.x > waypointPosition.x) {
                unitDirection = Constants.PathfindingDirections.Left;
            }
        }
    }

    public void OnDrawGizmos() {
        if (currentPath != null) {
            for (int i = targetIndex; i < currentPath.Length; i++) {
                if (onGoalPath && !onTargetPath && !onTrapPath) {
                    Gizmos.color = goalPathColor;
                } else if (!onGoalPath && onTargetPath && !onTrapPath) {
                    Gizmos.color = targetPathColor;
                } else if (!onGoalPath && !onTargetPath && onTrapPath) {
                    Gizmos.color = trapPathColor;
                }
                Gizmos.DrawCube(currentPath[i], new Vector2(0.5f, 0.5f));

                if (i == targetIndex) {
                    Gizmos.DrawLine(transform.position, currentPath[i]);
                } else {
                    Gizmos.DrawLine(currentPath[i - 1], currentPath[i]);
                }
            }
        }
    }
}
