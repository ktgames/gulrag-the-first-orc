﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapBaseScript : MonoBehaviour {

    [SerializeField] float initialTrapHealth;
    [SerializeField] public float trapHealth;
    [SerializeField] bool beingDisarmed = false;
    [SerializeField] GameObject beingDisarmedBy;
    [SerializeField] bool disarmed = false;
    GameObject enemyToAttack;
    float attackRadius;
    [SerializeField] Color color; 

	Animator animator; 

	void Start () {
        trapHealth = initialTrapHealth;
		animator = GetComponent<Animator>();
	}
	
	void Update () {
        //if (enemyToAttack != null) {
        //    SetAttackDistanceAndAttack();
        //}
        
        if (trapHealth <= 0) {
            Death();
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == Constants.DETECTION_COLLIDER
                && collision is CircleCollider2D
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>() != null
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().unitType == UnitBase.UnitTypes.Attacker
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Stealth
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().currentTrap == null
                && !collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().trapFound) {
            collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().trapFound = true;
            collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().currentTrap = this.gameObject;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().trapCollision = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().trapTransform = this.gameObject.transform;
        } else if (collision.gameObject.name == Constants.BODY_COLLIDER
                && collision is BoxCollider2D
                && collision.gameObject.transform.parent != null
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>() != null
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().unitType == UnitBase.UnitTypes.Attacker
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().unitMainAttackType == UnitBase.UnitAttackTypes.Stealth
                && collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().currentTrap == this.gameObject) {
            beingDisarmed = true;
            beingDisarmedBy = collision.gameObject.GetComponent<AttackerCollisionDetection>().parentGameObject;
            collision.gameObject.transform.parent.GetComponent<StealthMeleeAttackerBase>().disarmingTrap = true;
            collision.gameObject.transform.parent.GetComponent<UnitPathfinding>().pathfindingPaused = true;
        }
    }

    void Death() {
        Destroy(this.gameObject);
    }

    void SetAttackDistanceAndAttack() {
		float distanceToEnemy = Vector2.Distance(enemyToAttack.transform.position, this.transform.position);
		if(distanceToEnemy < attackRadius) {
			animator.SetTrigger("CLAMPING");
		} 
	}
	
	void OnDrawGizmos() {	   
	    Gizmos.color = color; 
	    Gizmos.DrawWireSphere(transform.position, attackRadius); 
	}
}
