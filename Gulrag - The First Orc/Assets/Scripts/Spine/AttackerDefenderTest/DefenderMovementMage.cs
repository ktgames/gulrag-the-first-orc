﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class DefenderMovementMage : MonoBehaviour {

    [SerializeField] float horizontalMovementSpeed;
    [SerializeField] float verticalMovementSpeed;
    public AnimationReferenceAsset idle;
    public AnimationReferenceAsset orbitingOrbs;
    public AnimationReferenceAsset floatUpForWalk;
    public AnimationReferenceAsset floatTowardsForWalk;
    public AnimationReferenceAsset floatDownForNextMove;
    public AnimationReferenceAsset fireBall;
    public AnimationReferenceAsset deathAnimation;


    public SkeletonAnimation skeletonAnimation;
    bool facingRight = true;

    void Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true);
        skeletonAnimation.AnimationState.SetAnimation(0, orbitingOrbs, true);
    }

    void LateUpdate()
    {

        MoveCharacterAndLockAxis();
        FlipCharacter();
        SpineAnimationsTesting();

        //skeletonAnimation.AnimationState.SetAnimation(0, "Walk Animation", true); did not work on late update
        // the spine user guide states that it works on start and should be updated in start, I need to test to see
        //if switching states can be done in seperate method calls, update functions, etc. I think it will be after
        //The initial animation state has been set at the start of the game. We do need to avoid Awake as this is when
        //Spine itself instantiates the animations so if we compete on awake we could break the animations. 
    }

    void FlipCharacter()
    {

        if (Input.GetKey(KeyCode.A) && facingRight == true)
        {
            skeletonAnimation.Skeleton.FlipX = true;
            //currently flips the player and moves him really far fuck you SPine
            //manually rotating his scale from .3 to -.3 still moves the player far to the left. After moving
            //The players scale to 1 across the board to get his original size he still moves hard to the left. 
            //Tested to see if flipping character was causing fake collider issues where he cant move randomly on the map
            //This is the case as commenting out code prevents this. 
            facingRight = false;
            Debug.Log(facingRight);
        }

        if (Input.GetKey(KeyCode.D) && facingRight == false)
        {
            skeletonAnimation.Skeleton.FlipX = false;
            facingRight = true;
            Debug.Log(facingRight);
        }

    }

    void MoveCharacterAndLockAxis()
    {
        float moveHorizontal = Input.GetAxis("Horizontal") * horizontalMovementSpeed;
        float moveVertical = Input.GetAxis("Vertical") * verticalMovementSpeed;

        if (Input.GetKey(KeyCode.S))
        {
            skeletonAnimation.AnimationState.SetAnimation(1, floatTowardsForWalk, true);
            //skeletonAnimation.Skeleton.SetSkin("Gurlag_Facing_Down"); //I did spell gulrag wrong
            skeletonAnimation.Skeleton.SetSlotsToSetupPose();
            Vector2 movement = new Vector2(0, moveVertical);
            GetComponent<Rigidbody2D>().velocity = movement;
        }
        if (Input.GetKey(KeyCode.W))
        {
            skeletonAnimation.AnimationState.SetAnimation(1, floatTowardsForWalk, true);
            // skeletonAnimation.Skeleton.SetSkin("Gulrag_Facing_Forward"); //I did spell gulrag wrong
            skeletonAnimation.Skeleton.SetSlotsToSetupPose();
            Vector2 movement = new Vector2(0, moveVertical);
            GetComponent<Rigidbody2D>().velocity = movement;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            skeletonAnimation.AnimationState.SetAnimation(1, floatTowardsForWalk, true);
            // skeletonAnimation.Skeleton.SetSkin("Gulrag_Facing_Right");
            skeletonAnimation.Skeleton.SetSlotsToSetupPose();
            Vector2 movement = new Vector2(moveHorizontal, 0);
            GetComponent<Rigidbody2D>().velocity = movement;
        }
    }

    void SpineAnimationsTesting()
    {

        if (Input.GetKey(KeyCode.Space))
        {

            skeletonAnimation.AnimationState.SetAnimation(1, fireBall, false);
        }
        if (Input.GetKey(KeyCode.V))
        {
            skeletonAnimation.AnimationState.SetAnimation(1, deathAnimation, false);
        }
    }
}

