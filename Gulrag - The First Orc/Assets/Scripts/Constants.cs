﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {

    public static string DETECTION_COLLIDER = "Detection Collider";
    public static string BODY_COLLIDER = "Body Collider";

    public enum PathfindingDirections { Up, Down, Left, Right }
}
