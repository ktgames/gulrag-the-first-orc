﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections; 

public class GameManager : MonoBehaviour {

    [SerializeField] int playerStartingCurrency = 1000;
    [SerializeField] public static int playerCurrency;

    [SerializeField] Text countText;
    [SerializeField] Text winText;
    [SerializeField] GameObject startButton;
    [SerializeField] GameObject optionsButton;
    [SerializeField] GameObject exitButton;
    [SerializeField] GameObject audioSlider;
    [SerializeField] GameObject difficultySlider;
    [SerializeField] GameObject rawDifficultyText;
    [SerializeField] GameObject rawAudioLevelText;
    [SerializeField] GameObject mainMenuButton;
    [SerializeField] GameObject pauseButton;
    [SerializeField] GameObject resumeButton;
    [SerializeField] GameObject trollhunterIcon;
    [SerializeField] GameObject prototypeTrollHunter;
    [SerializeField] GameObject goblinRogueIcon;
    [SerializeField] GameObject prototypeGoblinRogue;
    [SerializeField] GameObject gulragIcon;
    [SerializeField] GameObject prototypeGulrag;
    [SerializeField] GameObject prototypeUndeadHealer;
    [SerializeField] GameObject undeadhealerIcon; 
    [SerializeField] Canvas canvas;
    [SerializeField] GameObject realCanvas;
    [SerializeField] Camera mainCamera; 

    
    bool isReadyToPlaceAttacker = false;
    int trollHunterCost = 200;
    int goblinRogueCost = 150;
    int undeadHealerCost = 100; 
    int orcWarriorCost = 100;
    int sceneBuildIndex = 0;

    void Start() {
        playerCurrency = playerStartingCurrency;
        SetCountText();
        winText.enabled = false;
    }

    void Update () {
        CalculatePlayerCurrency();
        SetCountText();

        int sceneManagerBuildIndex = SceneManager.GetActiveScene().buildIndex;

        if (sceneManagerBuildIndex == 0)
        {
            StartCoroutine(CountDownToNextScene());
        }

        if (goblinRogueIcon.GetComponent<Image>().color == Color.black && isReadyToPlaceAttacker == true && Input.GetMouseButtonDown(0)/* && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()*/)
        {
            InstantiateAttackers();
        }

        OnMouseOver();
    }

    void OnMouseOver()
    {

        var mousePosition = Input.mousePosition;
        mousePosition.z = 10;
       //Debug.Log(mainCamera.ScreenToWorldPoint(mousePosition));

    }

    public int UpdatePlayerCurrency(UnitBase.UnitTypes unitType, int deathValue) {

        if (unitType == UnitBase.UnitTypes.Defender) {
            playerCurrency += deathValue;
        }
        return playerCurrency;
    }

    public float CalculatePlayerCurrency() {
        SetCountText();
        return playerCurrency; 
    }

    void SetCountText() {
        countText.text = "Currency: " + playerCurrency.ToString();
    }

    public void SetWinText()
    {
        winText.enabled = true; 
    }

    IEnumerator CountDownToNextScene()
    {
            yield return new WaitForSecondsRealtime(5);
            sceneBuildIndex = 1;
            SceneManager.LoadSceneAsync(sceneBuildIndex);
    }

    public void LoadNextScene()
    {
        SceneManager.LoadSceneAsync(sceneBuildIndex + 2);
    }

    public void Quit()
    {
        EditorApplication.isPlaying = false; 
    }

    public void OptionsButton()
    {
        startButton.SetActive(false);
        optionsButton.SetActive(false);
        exitButton.SetActive(false);

        audioSlider.SetActive(true);
        difficultySlider.SetActive(true);
        rawDifficultyText.SetActive(true);
        rawAudioLevelText.SetActive(true);
        mainMenuButton.SetActive(true);
    }

    public void MainMenuButtonClicked()
    {
        startButton.SetActive(true);
        optionsButton.SetActive(true);
        exitButton.SetActive(true);

        audioSlider.SetActive(false);
        difficultySlider.SetActive(false);
        rawDifficultyText.SetActive(false);
        rawAudioLevelText.SetActive(false);
        mainMenuButton.SetActive(false);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        pauseButton.SetActive(false);
        resumeButton.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        resumeButton.SetActive(false);
        pauseButton.SetActive(true);
    }

    public void ReadyButtonForInstantiatingAttackers()
    {
        //gulragIcon.GetComponent<Image>().color = Color.black;
        goblinRogueIcon.GetComponent<Image>().color = Color.black;
        isReadyToPlaceAttacker = true;
    }
     
    private void InstantiateAttackers()
    {
        var mousePosition = Input.mousePosition;
        mousePosition.z = 10; // I needed to force the mouse position.z ten units away from the canvas because i was getting the cameras raycast position not the cursors. 
        Debug.Log(mainCamera.ScreenToWorldPoint(mousePosition));

        if (playerCurrency >= undeadHealerCost)
        {
            //Instantiate(prototypeGulrag, mainCamera.ScreenToWorldPoint(mousePosition), Quaternion.identity);
            Instantiate(prototypeGoblinRogue, mainCamera.ScreenToWorldPoint(mousePosition), Quaternion.identity);
            isReadyToPlaceAttacker = false;
            //gulragIcon.GetComponent<Image>().color = Color.white;
            goblinRogueIcon.GetComponent<Image>().color = Color.white;
            //playerCurrency -= orcWarriorCost; // Get this cost from the unit class eventually.
            playerCurrency -= goblinRogueCost; 
        }
        else
        {
            Debug.Log("You have not enough minerals");
            goblinRogueIcon.GetComponent<Image>().color = Color.white; 
            //gulragIcon.GetComponent<Image>().color = Color.white; 
        }
    }
}
