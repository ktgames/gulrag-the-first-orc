
Tauren Healer.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Cloak
  rotate: false
  xy: 2, 42
  size: 147, 212
  orig: 147, 212
  offset: 0, 0
  index: -1
Head
  rotate: true
  xy: 333, 101
  size: 67, 92
  orig: 67, 92
  offset: 0, 0
  index: -1
Left_Bicep
  rotate: true
  xy: 278, 24
  size: 33, 46
  orig: 33, 46
  offset: 0, 0
  index: -1
Left_Forearm
  rotate: false
  xy: 322, 75
  size: 33, 24
  orig: 33, 24
  offset: 0, 0
  index: -1
Left_Hand_And_Staff
  rotate: false
  xy: 151, 121
  size: 180, 133
  orig: 180, 133
  offset: 0, 0
  index: -1
Left_Hoof
  rotate: true
  xy: 479, 221
  size: 33, 31
  orig: 33, 31
  offset: 0, 0
  index: -1
Right_Hoof
  rotate: true
  xy: 479, 221
  size: 33, 31
  orig: 33, 31
  offset: 0, 0
  index: -1
Left_Shin
  rotate: false
  xy: 427, 105
  size: 36, 63
  orig: 36, 63
  offset: 0, 0
  index: -1
Left_Shoulder
  rotate: false
  xy: 116, 5
  size: 33, 35
  orig: 33, 35
  offset: 0, 0
  index: -1
Left_Thigh
  rotate: true
  xy: 206, 21
  size: 36, 70
  orig: 36, 70
  offset: 0, 0
  index: -1
Neck
  rotate: true
  xy: 479, 185
  size: 34, 26
  orig: 34, 26
  offset: 0, 0
  index: -1
Right_Bicep
  rotate: false
  xy: 82, 2
  size: 32, 38
  orig: 32, 38
  offset: 0, 0
  index: -1
Right_Forearm
  rotate: true
  xy: 357, 73
  size: 26, 30
  orig: 26, 30
  offset: 0, 0
  index: -1
Right_Hand
  rotate: true
  xy: 389, 72
  size: 27, 28
  orig: 27, 28
  offset: 0, 0
  index: -1
Right_Shin
  rotate: false
  xy: 287, 59
  size: 33, 60
  orig: 33, 60
  offset: 0, 0
  index: -1
Right_Shoulder_Pad
  rotate: true
  xy: 151, 2
  size: 55, 53
  orig: 55, 53
  offset: 0, 0
  index: -1
Right_Thigh
  rotate: true
  xy: 2, 4
  size: 36, 78
  orig: 36, 78
  offset: 0, 0
  index: -1
Skirt
  rotate: true
  xy: 333, 170
  size: 84, 144
  orig: 84, 144
  offset: 0, 0
  index: -1
Torso
  rotate: true
  xy: 151, 59
  size: 60, 134
  orig: 60, 134
  offset: 0, 0
  index: -1
